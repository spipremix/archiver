<?php

namespace SpipRemix\Archiver\Tests;

use PHPUnit\Framework\TestCase;
use SpipRemix\Archiver\SpipArchiver;

/**
 * @covers \SpipRemix\Archiver\AbstractArchiver
 * @covers \SpipRemix\Archiver\SpipArchiver
 * @covers \SpipRemix\Archiver\ZipArchive
 * @covers \SpipRemix\Archiver\TarArchive
 * @covers \SpipRemix\Archiver\TgzArchive
 * @covers \SpipRemix\Archiver\NoDotFilterIterator
 *
 * @internal
 */
class SpipArchiverTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        @mkdir(__DIR__ . '/../var/tmp/directory');
        @file_put_contents(__DIR__ . '/../var/tmp/directory/test.txt', 'contenu de test');
    }

    public static function tearDownAfterClass(): void
    {
        @unlink(__DIR__ . '/../var/tmp/test.txt');
        @unlink(__DIR__ . '/../var/tmp/tar/directory/test.txt');
        @rmdir(__DIR__ . '/../var/tmp/tar/directory');
        @unlink(__DIR__ . '/../var/tmp/tgz/directory/test.txt');
        @rmdir(__DIR__ . '/../var/tmp/tgz/directory');
        @unlink(__DIR__ . '/../var/tmp/directory/test.txt');
        @rmdir(__DIR__ . '/../var/tmp/directory');
        @unlink(__DIR__ . '/../var/tmp/emballer.zip');
        @unlink(__DIR__ . '/../var/tmp/emballer2.zip');
    }

    public function dataInformer()
    {
        return [
            'empty-string' => [
                3,
                [
                    'proprietes' => [],
                    'fichiers' => [],
                ],
                '',
                '',
            ],
            'unknown' => [
                2,
                [
                    'proprietes' => [],
                    'fichiers' => [],
                ],
                __DIR__ . '/../var/tmp/file.unknown',
                '',
            ],
            'exotic' => [
                2,
                [
                    'proprietes' => [],
                    'fichiers' => [],
                ],
                __DIR__ . '/../var/tmp/file.unknown',
                'exotic',
            ],
            'zip' => [
                0,
                [
                    'proprietes' => [
                        'racine' => '',
                    ],
                    'fichiers' => [
                        [
                            'filename' => 'test.txt',
                            'size' => 16,
                        ],
                    ],
                ],
                __DIR__ . '/../var/tmp/test.zip',
                '',
            ],
            'tar' => [
                0,
                [
                    'proprietes' => [
                        'racine' => '/directory/',
                    ],
                    'fichiers' => [
                        [
                            'filename' => '/directory/test.txt',
                            'size' => 16,
                        ],
                    ],
                ],
                __DIR__ . '/../var/tmp/tar/test.tar',
                '',
            ],
            'tgz' => [
                0,
                [
                    'proprietes' => [
                        'racine' => '/directory/',
                    ],
                    'fichiers' => [
                        [
                            'filename' => '/directory/test.txt',
                            'size' => 16,
                        ],
                    ],
                ],
                __DIR__ . '/../var/tmp/tgz/test.tar.gz',
                '',
            ],
        ];
    }

    /**
     * @dataProvider dataInformer
     */
    public function testInformer($expected, $expectedList, $file, $extension)
    {
        // Given
        $archiver = new SpipArchiver($file, $extension);

        // When
        $actual = $archiver->informer();

        //Then
        $this->assertEquals($expectedList, $actual);
        $this->assertEquals($expected, $archiver->erreur());
    }

    public function dataDeballer()
    {
        return [
            'zip' => [
                true,
                0,
                __DIR__ . '/../var/tmp/test.zip',
                __DIR__ . '/../var/tmp',
                [],
                __DIR__ . '/../var/tmp/test.txt',
            ],
            'tar' => [
                true,
                0,
                __DIR__ . '/../var/tmp/tar/test.tar',
                __DIR__ . '/../var/tmp/tar',
                [],
                __DIR__ . '/../var/tmp/tar/directory/test.txt',
            ],
            'tgz' => [
                true,
                0,
                __DIR__ . '/../var/tmp/tgz/test.tar.gz',
                __DIR__ . '/../var/tmp/tgz',
                ['directory/test.txt'],
                __DIR__ . '/../var/tmp/tgz/directory/test.txt',
            ],
        ];
    }

    /**
     * @dataProvider dataDeballer
     */
    public function testDeballer($expected, $expectedErreur, $file, $target, $files, $testFile)
    {
        // Given
        $archiver = new SpipArchiver($file);

        // When
        $actual = $archiver->deballer($target, $files);

        //Then
        $this->assertEquals($expected, $actual, 'decompress ok');
        $this->assertEquals($expectedErreur, $archiver->erreur(), 'error code');
        $this->assertTrue(file_exists($testFile), 'decompressed file exists');
    }

    public function dataEmballer()
    {
        return [
            'zip' => [
                true,
                0,
                __DIR__ . '/../var/tmp/emballer.zip',
                '',
                [__DIR__ . '/../var/tmp/directory/test.txt'],
                __DIR__ . '/../var/tmp/directory',
            ],
            'zip2' => [
                true,
                0,
                __DIR__ . '/../var/tmp/emballer2.zip',
                '',
                [__DIR__ . '/../var/tmp/directory/test.txt'],
                null,
            ],
        ];
    }

    /**
     * @dataProvider dataEmballer
     */
    public function testEmballer($expected, $expectedErreur, $file, $extension, $files, $target)
    {
        // Given
        $archiver = new SpipArchiver($file, $extension);

        // When
        $actual = $archiver->emballer($files, $target);

        //Then
        $this->assertEquals($expected, $actual, 'compress ok');
        $this->assertEquals($expectedErreur, $archiver->erreur(), 'error code');
        $this->assertTrue(file_exists($file), 'compressed file exists');
    }
}
